package com.pawelbanasik;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) throws FileNotFoundException {

		String fileName = "dane.txt";
		File textFile = new File(fileName);
		Scanner scanner = new Scanner(textFile);
		
		String value = scanner.next();
		System.out.println("Read value: " + value);
		
		scanner.nextLine();
		
		int count = 2;
		
		while (scanner.hasNextLine()) {
			
			String line = scanner.nextLine();
			
			System.out.println(count + ": " + line);
			count++;
		}
		scanner.close();
		
	}

}
